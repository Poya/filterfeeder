﻿using System.Data.Linq;

namespace FilterFeeder.Domain
{
    public class FeedDataContext : DataContext
    {
        public FeedDataContext(string connectionString) : base(connectionString)
        {
        }

        public Table<FeedSource> FeedSources
        {
            get { return GetTable<FeedSource>(); }
        }

        public Table<FeedFilter> FeedFilters
        {
            get { return GetTable<FeedFilter>(); }
        }

        protected Table<FeedFilterSource> FeedFilterSources
        {
            get { return GetTable<FeedFilterSource>(); }
        }
    }
}
