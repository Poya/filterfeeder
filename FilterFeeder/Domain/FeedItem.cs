﻿using System;

namespace FilterFeeder.Domain
{
    public class FeedItem
    {
        public FeedItem(FeedSource source, string title, string imageSource, DateTime publishDate)
        {
            Source = source;
            Title = title;
            ImageSource = imageSource;
            PublishDate = publishDate;
        }

        public FeedSource Source { get; private set; }
        public string Title { get; private set; }
        public string ImageSource { get; private set; }
        public DateTime PublishDate { get; private set; }
    }
}
