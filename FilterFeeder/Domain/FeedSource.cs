﻿using System.Data.Linq.Mapping;

namespace FilterFeeder.Domain
{
    [Table]
    public class FeedSource
    {
        public FeedSource()
        {
        }

        public FeedSource(string name, string url)
        {
            Name = name;
            Url = url;
        }

        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int Id { get; protected set; }

        [Column(CanBeNull = false)]
        public string Name { get; protected set; }

        [Column(CanBeNull = false)]
        public string Url { get; protected set; }
    }
}
