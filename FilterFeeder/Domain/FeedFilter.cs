﻿using System.Collections.Generic;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;

namespace FilterFeeder.Domain
{
    [Table]
    public class FeedFilter
    {
        public FeedFilter()
        {
        }

        public FeedFilter(string name, IEnumerable<FeedSource> sources, IEnumerable<string> filterWords)
        {
            Name = name;
            FeedSources = new EntitySet<FeedFilterSource>();
            FeedSources.AddRange(sources.Select(s => new FeedFilterSource(Id, s.Id)));
            FilterWords = filterWords;
        }

        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int Id { get; private set; }

        [Column(CanBeNull = true)]
        public string Name { get; private set; }

        [Association(ThisKey = "Id", OtherKey = "FeedSourceId")]
        public EntitySet<FeedFilterSource> FeedSources { get; private set; }
        public IEnumerable<string> FilterWords { get; private set; }

        public IEnumerable<FeedItem> FilterItems(IEnumerable<FeedItem> feedItems)
        {
            return feedItems.Where(item => 
                FeedSources.Any(x => x.FeedSource.Url == item.Source.Url));
        }
    }
}
