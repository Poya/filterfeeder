﻿using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace FilterFeeder.Domain
{
    [Table]
    public class FeedFilterSource
    {
        public FeedFilterSource()
        {
        }

        public FeedFilterSource(int filterId, int sourceId)
        {
            FeedFilterId = filterId;
            FeedSourceId = sourceId;
        }

        [Column(IsPrimaryKey = true)]
        public int FeedFilterId { get; protected set; }

        [Column(IsPrimaryKey = true)]
        public int FeedSourceId { get; protected set; }

        private EntityRef<FeedFilter> _feedFilter;
        [Association(ThisKey = "FeedFilterId", OtherKey = "Id", Storage = "_feedFilter")]
        public FeedFilter FeedFilter
        {
            get { return _feedFilter.Entity; } 
            protected set { _feedFilter.Entity = value; }
        }

        private EntityRef<FeedSource> _feedSource;
        [Association(ThisKey = "FeedSourceId", OtherKey = "Id", Storage = "_feedSource")]
        public FeedSource FeedSource
        {
            get { return _feedSource.Entity; }
            protected set { _feedSource.Entity = value; }
        }
    }
}
