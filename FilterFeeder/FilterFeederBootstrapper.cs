﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using Caliburn.Micro;
using FilterFeeder.Domain;
using FilterFeeder.Services;
using FilterFeeder.ViewModels;
using Microsoft.Phone.Controls;

namespace FilterFeeder
{
    public class FilterFeederBootstrapper : PhoneBootstrapper
    {
        private const string ConnectionString = "isostore:/filterfeeder.sdf";

        private PhoneContainer _container;

        protected override void Configure()
        {
            _container = new PhoneContainer(RootFrame);

            _container.RegisterPhoneServices();
            _container.PerRequest<MainPageViewModel>();
            _container.PerRequest<FeedListViewModel>();
            _container.PerRequest<SourcesViewModel>();
            _container.PerRequest<FilterListViewModel>();
            _container.PerRequest<FilterViewModel>();
            _container.PerRequest<FilterSourceViewModel>();
            _container.Instance<IFeedService>(new FeedService(new FeedDataContext(ConnectionString)));

            InitializeDatabase();
        }

        private void InitializeDatabase()
        {
            using (var context = new FeedDataContext(ConnectionString))
            {
                context.DeleteDatabase();

                if (!context.DatabaseExists())
                {
                    context.CreateDatabase();
                    var source = new FeedSource("Gamasutra", "http://feeds.feedburner.com/GamasutraNews");
                    context.FeedSources.InsertOnSubmit(source);
                    context.SubmitChanges();
                }
            }
        }

        protected override void OnUnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            if (Debugger.IsAttached)
            {
                Debugger.Break();
                e.Handled = true;
            }
            else
            {
                MessageBox.Show("An unexpected error occured, sorry about the troubles.", "Oops...", MessageBoxButton.OK);
                e.Handled = true;
            }

            base.OnUnhandledException(sender, e);
        }

        protected override object GetInstance(Type service, string key)
        {
            return _container.GetInstance(service, key);
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return _container.GetAllInstances(service);
        }

        protected override void BuildUp(object instance)
        {
            _container.BuildUp(instance);
        }

        protected override PhoneApplicationFrame CreatePhoneApplicationFrame()
        {
            return new TransitionFrame();
        }
    }

}
