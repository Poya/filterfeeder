﻿using Caliburn.Micro;

namespace FilterFeeder.ViewModels
{
    public class KeywordSetViewModel : PropertyChangedBase
    {
        private string _keywords;
        public string Keywords
        {
            get { return _keywords; }
            set
            {
                _keywords = value;
                NotifyOfPropertyChange(() => Keywords);
            }
        }
    }
}
