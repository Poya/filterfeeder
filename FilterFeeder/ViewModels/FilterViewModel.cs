﻿using System.Collections.Generic;
using System.Linq;
using Caliburn.Micro;
using FilterFeeder.Domain;
using FilterFeeder.Services;

namespace FilterFeeder.ViewModels
{
    public class FilterViewModel : Screen
    {
        private readonly IFeedService _feedService;
        private FeedFilter _filter;

        public FilterViewModel(IFeedService feedService)
        {
            _feedService = feedService;
        }

        public int FilterId
        {
            get { return _filter == null ? 0 : _filter.Id; }
            set
            {
                _filter = _feedService.GetFilter(value);
                Name = _filter.Name;

                if (_filter.FilterWords != null)
                {
                    KeywordSets = new BindableCollection<KeywordSetViewModel>();
                    KeywordSets.AddRange(_filter.FilterWords.Select(x => new KeywordSetViewModel {Keywords = x}));
                }

                UpdateSources();
            }
        }

        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                NotifyOfPropertyChange(() => Name);
            }
        }

        private IList<FilterSourceViewModel> _sources;
        public IList<FilterSourceViewModel> Sources
        {
            get { return _sources; }
            set
            {
                _sources = value;
                NotifyOfPropertyChange(() => Sources);
            }
        }

        private IObservableCollection<KeywordSetViewModel> _keywordSets = new BindableCollection<KeywordSetViewModel>();
        public IObservableCollection<KeywordSetViewModel> KeywordSets
        {
            get { return _keywordSets; }
            set
            {
                _keywordSets = value;
                NotifyOfPropertyChange(() => KeywordSets);
            }
        }

        private string _lastKeywordSet;
        public string LastKeywordSet
        {
            get { return _lastKeywordSet; }
            set
            {
                _lastKeywordSet = value;
                NotifyOfPropertyChange(() => LastKeywordSet);
            }
        }

        public void Save()
        {
            if (_filter == null)
            {
                _feedService.AddFilter(new FeedFilter(
                    Name, Sources.Where(s => s.IsSelected).Select(s => s.Source), KeywordSets.Select(x => x.Keywords)
                ));
            }
            else
            {
            }
        }

        public void AddKeywordSet()
        {
            KeywordSets.Add(new KeywordSetViewModel { Keywords = LastKeywordSet });
            LastKeywordSet = null;
        }

        public void RemoveKeywordSet(KeywordSetViewModel vm)
        {
            KeywordSets.Remove(vm);
        }

        #region Overrides

        protected override void OnInitialize()
        {
            base.OnInitialize();

            UpdateSources();
        }

        #endregion

        #region Private Methods

        private void UpdateSources()
        {
            Sources = _feedService.GetSources()
                .Select(s =>
                {
                    var vm = new FilterSourceViewModel(s);
                    vm.IsSelected = (_filter != null && _filter.FeedSources.Any(x => x.FeedSource.Url == s.Url));
                    return vm;
                })
                .ToList();
        }

        #endregion
    }
}