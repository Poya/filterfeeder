﻿using System.Threading.Tasks;
using Caliburn.Micro;
using FilterFeeder.Domain;
using FilterFeeder.Services;

namespace FilterFeeder.ViewModels
{
    public class SourcesViewModel : Screen
    {
        private readonly IFeedService _feedService;

        public SourcesViewModel(IFeedService feedService)
        {
            _feedService = feedService;
        }

        public IObservableCollection<FeedSource> Sources
        {
            get
            {
                return new BindableCollection<FeedSource>(_feedService.GetSources());
            }
        }

        private string _url;
        public string Url
        {
            get
            {
                return _url;
            }
            set
            {
                _url = value;
                NotifyOfPropertyChange(() => Url);
            }
        }

        public async Task AddFeed()
        {
            await _feedService.AddSource(Url);
            NotifyOfPropertyChange(() => Sources);
        }
    }
}
