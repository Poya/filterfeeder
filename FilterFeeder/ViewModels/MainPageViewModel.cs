﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Caliburn.Micro;
using FilterFeeder.Services;

namespace FilterFeeder.ViewModels
{
    public class MainPageViewModel : Screen
    {
        private readonly IFeedService _feedService;
        private readonly INavigationService _navigationService;

        public MainPageViewModel(IFeedService feedService, INavigationService navigationService)
        {
            _feedService = feedService;
            _navigationService = navigationService;
        }

        private FeedListViewModel _allFeedsList;
        public FeedListViewModel AllFeedsList
        {
            get { return _allFeedsList; }
            set
            {
                _allFeedsList = value;
                NotifyOfPropertyChange(() => AllFeedsList);
            }
        }

        private IList<FeedListViewModel> _filteredFeedLists = new ObservableCollection<FeedListViewModel>();
        public IList<FeedListViewModel> FilteredFeedLists
        {
            get { return _filteredFeedLists; }
            set
            {
                _filteredFeedLists = value;
                NotifyOfPropertyChange(() => FilteredFeedLists);
            }
        }

        protected override void OnActivate()
        {
         	base.OnActivate();

            AllFeedsList = new FeedListViewModel(_feedService, null);
            AllFeedsList.ConductWith(this);

            var filters = _feedService.GetFilters();
            FilteredFeedLists.Clear();
            foreach (var filter in filters)
            {
                var vm = new FeedListViewModel(_feedService, filter);
                FilteredFeedLists.Add(vm);
                vm.ConductWith(this);
            }
        }

        public void ShowSources()
        {
            _navigationService.UriFor<SourcesViewModel>()
                .Navigate();
        }

        public void ShowFilters()
        {
            _navigationService.UriFor<FilterListViewModel>()
                .Navigate();
        }
    }
}
