﻿using Caliburn.Micro;
using FilterFeeder.Domain;

namespace FilterFeeder.ViewModels
{
    public class FilterSourceViewModel : PropertyChangedBase
    {
        public FilterSourceViewModel(FeedSource source)
        {
            Source = source;
        }

        public FeedSource Source { get; private set; }

        public string Name
        {
            get { return Source.Name; }
        }

        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                NotifyOfPropertyChange(() => IsSelected);
            }
        }
    }
}
