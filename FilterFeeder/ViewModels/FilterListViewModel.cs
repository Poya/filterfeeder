﻿using System.Collections.Generic;
using Caliburn.Micro;
using FilterFeeder.Domain;
using FilterFeeder.Services;

namespace FilterFeeder.ViewModels
{
    public class FilterListViewModel : Screen
    {
        private readonly IFeedService _feedService;
        private readonly INavigationService _navigationService;

        public FilterListViewModel(IFeedService feedService, INavigationService navigationService)
        {
            _feedService = feedService;
            _navigationService = navigationService;
        }

        public IList<FeedFilter> Filters
        {
            get { return _feedService.GetFilters(); }
        }

        public void AddFilter()
        {
            _navigationService.UriFor<FilterViewModel>()
                .Navigate();
        }

        public void EditFilter(FeedFilter filter)
        {
            _navigationService.UriFor<FilterViewModel>()
                .WithParam(x => x.FilterId, filter.Id)
                .Navigate();
        }

        protected override void OnActivate()
        {
            base.OnActivate();

            NotifyOfPropertyChange(() => Filters);
        }
    }
}
