﻿using System.Collections.Generic;
using System.Linq;
using Caliburn.Micro;
using FilterFeeder.Domain;
using FilterFeeder.Services;

namespace FilterFeeder.ViewModels
{
    public class FeedListViewModel : Screen
    {
        private readonly IFeedService _feedService;
        private readonly FeedFilter _filter;

        public FeedListViewModel(IFeedService feedService, FeedFilter filter)
        {
            _feedService = feedService;
            _filter = filter;
        }

        public string Title { get { return _filter == null ? null : _filter.Name; } }

        private IList<FeedItem> _feedItems;
        public IList<FeedItem> FeedItems
        {
            get
            {
                if (_feedItems == null)
                    return null;

                return _filter == null ? _feedItems : _filter.FilterItems(_feedItems).ToList();
            }
            set
            {
                _feedItems = value;
                NotifyOfPropertyChange(() => FeedItems);
            }
        }

        protected async override void OnInitialize()
        {
            base.OnInitialize();

            FeedItems = await _feedService.GetFeedItems();
        }
    }
}
