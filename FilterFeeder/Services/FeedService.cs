﻿using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.ServiceModel.Syndication;
using System.Threading.Tasks;
using System.Xml;
using FilterFeeder.Domain;
using HtmlAgilityPack;

namespace FilterFeeder.Services
{
    public interface IFeedService
    {
        IList<FeedSource> GetSources();
        Task AddSource(string url);

        Task<IList<FeedItem>> GetFeedItems();

        IList<FeedFilter> GetFilters();
        FeedFilter GetFilter(int id);
        void AddFilter(FeedFilter filter);
    }

    public class FeedService : IFeedService
    {
        private readonly FeedDataContext _dataContext;

        public FeedService(FeedDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        private const string FeedSourcePrefix = "Feed Source:";
        private readonly IsolatedStorageSettings _settings = IsolatedStorageSettings.ApplicationSettings;

        public IList<FeedSource> GetSources()
        {
            return _dataContext.FeedSources.ToList();
        }

        public async Task AddSource(string url)
        {
            var feed = await LoadFeed(url);
            _settings[FeedSourcePrefix + feed.Title.Text] = url;
        }

        public async Task<IList<FeedItem>> GetFeedItems()
        {
            var feedSources = GetSources();
            var feedItems = new List<FeedItem>();

            foreach (var source in feedSources)
            {
                var feed = await LoadFeed(source.Url);

                feedItems.AddRange(feed.Items
                    .Select(i => CreateFeedItem(source, i))
                    .OrderByDescending(x => x.PublishDate)
                    .ToList());
            }

            return feedItems;
        }

        public IList<FeedFilter> GetFilters()
        {
            return _dataContext.FeedFilters
                .OrderBy(f => f.Name)
                .ToList();
        }

        public FeedFilter GetFilter(int id)
        {
            return _dataContext.FeedFilters.SingleOrDefault(f => f.Id == id);
        }

        public void AddFilter(FeedFilter filter)
        {
            _dataContext.FeedFilters.InsertOnSubmit(filter);
            _dataContext.SubmitChanges();
        }

        #region Private Methods

        private static async Task<SyndicationFeed> LoadFeed(string url)
        {
            var client = new WebClient();
            var feedXml = await client.DownloadStringTaskAsync(url);

            var stringReader = new StringReader(feedXml);
            var xmlReader = XmlReader.Create(stringReader);
            
            return SyndicationFeed.Load(xmlReader);
        }

        private static FeedItem CreateFeedItem(FeedSource source, SyndicationItem syndicationItem)
        {
            var title = syndicationItem.Title.Text;
            var imageSource = GetImageSource(syndicationItem);
            var publishDate = syndicationItem.PublishDate.LocalDateTime;

            return new FeedItem(source, title, imageSource, publishDate);
        }

        private static string GetImageSource(SyndicationItem syndicationItem)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(syndicationItem.Summary.Text);
            var imageNode = doc.DocumentNode.Descendants("img").FirstOrDefault();
            return imageNode == null ? null : imageNode.Attributes["src"].Value;
        }

        #endregion
    }
}
